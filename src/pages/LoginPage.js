import React, { useState, useEffect } from "react";
import { useNavigate } from 'react-router-dom'
import { connect } from "react-redux";
import { bindActionCreators } from "@reduxjs/toolkit"
import { loginUser } from "../redux/actions/action_login_user";
import { resetVar } from "../redux/actions/action_login_user";
import "./loginPage.css"

const LoginPage = (props) => {
    const navigate = useNavigate();
    const [loginError, setloginError] = useState("");
    useEffect(() => {
        if (props.userDetails.error) {
            setloginError(props.userDetails.error_message);
        } else if (props.userDetails.post_login_success) {
            closeForm();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [props.userDetails.error, props.userDetails.post_login_success])


    const closeForm = () => {
        props.resetVar();
        navigate('/')
    }

    return (
        <>
            <div className=" d-flex justify-content-center align-items-center flex-column custom-login-form-box">
                <a href="/"><img className="mx-auto" src="/Wave_logo.svg" alt="Card" style={{ width: "150px" }} /></a>
                <h2>Sign in</h2>
                <form onSubmit={(e) => {
                    e.preventDefault();
                    let data = {};
                    data["userName"] = e.target.userName.value
                    data["password"] = e.target.password.value
                    props.loginUser(data);
                }}
                    className="custom-login-form">
                    <div className="form-group">
                        <input type="text" required className="form-control" id="userName" placeholder="Username" />
                    </div>
                    <div className="form-group">
                        <input type="password" required className="form-control" id="password" placeholder="Password" />
                    </div>
                    <label className="text-primary w-100">Forgot it?</label>
                    {loginError ? <label className="text-danger w-100">{loginError}</label> : <></>}
                    <button type="submit" className="btn btn-primary m-1  custom-login-button">Submit</button>
                    <button type="button" className="btn btn-secondary m-1 custom-login-button" onClick={closeForm}>Cancel</button>
                    <p>Don't have a Wave account yet? <a href="/signup">Sign up now.</a></p>
                </form>
            </div>
        </>
    )
}

const mapStateToProps = ({ userDetails }) => ({
    userDetails
})

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        loginUser,
        resetVar
    }, dispatch)

}
export default connect(mapStateToProps, mapDispatchToProps)(LoginPage)