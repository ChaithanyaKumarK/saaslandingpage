import React from "react";
import { Link } from "react-router-dom"

const ProductTab = (props) => {
    const { data } = props;
    const { image, title, description, id } = data;

    return (
        <>
            <div className="card py-3 h-100 align-self-stretch m-2" style={{ width: "18rem" }}>
                <img className="card-img-top mx-auto" src={image} alt="Card" style={{ width: "15vh", height: "20vh" }} />
                <div className="card-body">
                    <h5 className="card-title text-center text-truncate">{title}</h5>
                    <p className="card-text text-truncate" style={{}}>{description}</p>
                </div>
                <Link to={`/productDetails/${id}`} className="btn btn-dark w-75 mx-auto">Details</Link>
            </div>

        </>
    )
}

export default ProductTab