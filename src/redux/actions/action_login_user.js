import { startLoginUser, successLoginUser, error, reset, logout } from "../reducers/slice_user_details"

export const loginUser = (data) => {
    return (dispatch, getState) => {
        dispatch(startLoginUser())
        const state = getState((state) => state.userDetails)
        let success = isDataPresent(state, data)
        if (success) {
            dispatch(successLoginUser(data))
        } else {
            dispatch(error("Login failed. Enter valid credentials."))
        }
    }
}
export const resetVar = () => {
    return (dispatch) => {
        dispatch(reset())
    }
}
export const logoutUser = () => {
    return (dispatch) => {
        dispatch(logout())
    }
}
const isDataPresent = (state, data) => {
    let userFound = [];
    if (state.userDetails.data) {
        userFound = state.userDetails.data.filter((eachUser) => {
            if (eachUser.userName === data.userName && eachUser.password === data.password) {
                return true;
            }
            return false;
        })
    }
    if (userFound.length === 0) {
        return false;
    } else {
        return true;
    }
}