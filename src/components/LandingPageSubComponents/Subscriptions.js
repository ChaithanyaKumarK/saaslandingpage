import "./subComponents.css"



const Subscriptions = () => {

    return (
        <>
            <div className="container-fluid outer-0">
                <div className="d-flex align-items-center justify-content-center subscription-title">Solutions for every team</div>
                <div className="d-flex align-items-center justify-content-around outer-box flex-wrap">
                    <a href="/" className="card1 type1 flex1">
                        <h4 className="subscription-head">UX and Design</h4>
                        <span className="subscription-details">Understand needs and design solutions</span>
                        <span className="fa-svg-icon subscription-more">
                            Learn more <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M8 256c0 137 111 248 248 248s248-111 248-248S393 8 256 8 8 119 8 256zm448 0c0 110.5-89.5 200-200 200S56 366.5 56 256 145.5 56 256 56s200 89.5 200 200zm-107.5 8.5L225.7 387.3c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l91.7-91.7-91.7-91.7c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l122.8 122.8c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                        </span>
                    </a>
                    <a href="/" className="card1 type2 flex1">
                        <h4 className="subscription-head">UX and Design</h4>
                        <span className="subscription-details">Understand needs and design solutions</span>
                        <span className="fa-svg-icon subscription-more">
                            Learn more <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M8 256c0 137 111 248 248 248s248-111 248-248S393 8 256 8 8 119 8 256zm448 0c0 110.5-89.5 200-200 200S56 366.5 56 256 145.5 56 256 56s200 89.5 200 200zm-107.5 8.5L225.7 387.3c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l91.7-91.7-91.7-91.7c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l122.8 122.8c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                        </span>
                    </a>
                    <a href="/" className="card1 type3 flex1">
                        <h4 className="subscription-head">UX and Design</h4>
                        <span className="subscription-details">Understand needs and design solutions</span>
                        <span className="fa-svg-icon subscription-more">
                            Learn more <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M8 256c0 137 111 248 248 248s248-111 248-248S393 8 256 8 8 119 8 256zm448 0c0 110.5-89.5 200-200 200S56 366.5 56 256 145.5 56 256 56s200 89.5 200 200zm-107.5 8.5L225.7 387.3c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l91.7-91.7-91.7-91.7c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l122.8 122.8c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                        </span>
                    </a>
                    <a href="/" className="card1 type4 flex1">
                        <h4 className="subscription-head">UX and Design</h4>
                        <span className="subscription-details">Understand needs and design solutions</span>
                        <span className="fa-svg-icon subscription-more">
                            Learn more <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M8 256c0 137 111 248 248 248s248-111 248-248S393 8 256 8 8 119 8 256zm448 0c0 110.5-89.5 200-200 200S56 366.5 56 256 145.5 56 256 56s200 89.5 200 200zm-107.5 8.5L225.7 387.3c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l91.7-91.7-91.7-91.7c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l122.8 122.8c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                        </span>
                    </a>
                </div>
            </div>
        </>
    )

}

export default Subscriptions;