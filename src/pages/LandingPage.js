import React from "react";
import Hero from "../components/LandingPageSubComponents/Hero"
import Subscriptions from "../components/LandingPageSubComponents/Subscriptions";
import CompaniesList from "../components/LandingPageSubComponents/CompaniesList";
import CustomerReview from "../components/LandingPageSubComponents/CustomerReview";
import { connect } from "react-redux";
import { bindActionCreators } from "@reduxjs/toolkit"

const LandingPage = (props) => {
    return (
        <>
            <Hero />
            <Subscriptions />
            <CompaniesList />
            <CustomerReview />
        </>
    )
}

const mapStateToProps = (state) => ({
})

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
    }, dispatch)

}



export default connect(mapStateToProps, mapDispatchToProps)(LandingPage)