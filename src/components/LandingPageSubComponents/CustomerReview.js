import "./subComponents.css"



const Subscriptions = () => {

    return (
        <>
            <div className="container-fluid outer-0">
                <div className="d-flex align-items-center justify-content-center subscription-title">Loved by people and businesses around the world</div>
                <div className="d-flex align-items-center justify-content-around outer-box3 flex-wrap">
                    <div className="card card3">
                        <img className="card-img-top card3-img" src="/working3.jpg" alt="Card  cap1" />
                        <div className="card-body">
                            <h6 className="card-title">How Skyscanner rolled out a successful rebrand with Canva for Enterprise in 6 months</h6>

                        </div>
                    </div>
                    <div className="card card3">
                        <img className="card-img-top card3-img" src="/working3.jpg" alt="Card  cap1" />
                        <div className="card-body">
                            <h6 className="card-title">George Lee’s inspiring classroom collaboration story at Balboa High</h6>

                        </div>
                    </div>
                    <div className="card card3">
                        <img className="card-img-top card3-img" src="/working3.jpg" style={{ width: "auto" }} alt="Card  cap2" />
                        <div className="card-body">
                            <h6 className="card-title">Why Canva Pro’s quick “shareability” is its biggest drawcard for HuffPost</h6>

                        </div>
                    </div>
                    <div className="card card3">
                        <img className="card-img-top card3-img" src="/working3.jpg" alt="Card  cap3" />
                        <div className="card-body">
                            <h6 className="card-title">How HubSpot empowered 300 team members to move quickly, create on-brand designs and collaborate better</h6>

                        </div>
                    </div>
                </div>
            </div>
        </>
    )

}

export default Subscriptions;