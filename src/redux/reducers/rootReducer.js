import { configureStore } from '@reduxjs/toolkit'
import userDetailsReducer from './slice_user_details'

export const store = configureStore({
  reducer: {
    userDetails: userDetailsReducer
  },
})