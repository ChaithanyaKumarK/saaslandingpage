import { startInsertUserDetails, successInsertUserDetails } from "../reducers/slice_user_details"

export const signUpUser = (data) => {
    return (dispatch) => {
        dispatch(startInsertUserDetails())
        dispatch(successInsertUserDetails(data))
    }
}